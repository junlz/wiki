> <h3>二十六史</h3>

##### 01·[汉]司马迁·史记
##### 02·[汉]班固·汉书
##### 03·[南朝宋]范晔·后汉书
##### 04·[晋]陈寿·三国志
##### 05·[唐]房玄龄·晋书
##### 06·[南朝梁]沈约·宋书
##### 07·[南朝梁]萧子显·南齐书
##### 08·[唐]姚思廉·梁书
##### 09·[唐]姚思廉·陈书
##### 10·[北齐]魏收·魏书
##### 11·[唐]李百药·北齐书
##### 12·[唐]令狐德棻·周书
##### 13·[唐]魏征等·隋书
##### 14·[唐]李延寿·南史
##### 15·[唐]李延寿·北史
##### 16·[后晋]刘昫等·旧唐书
##### 17·[宋]欧阳修、宋祁等·新唐书
##### 18·[宋]薛居正等·旧五代史
##### 19·[宋]欧阳修·新五代史
##### 20·[元]脱脱等·宋史
##### 21·[元]脱脱等·辽史
##### 22·[元]脱脱等·金史
##### 23·[明]宋濂等·元史
##### 24·[清]张廷玉等·明史
##### 25·[民国]柯劭忞·新元史
##### 26·[民国]赵尔巽任、缪荃孙、柯劭忞等·请史稿


> <h3>诸子百家</h3>

##### 儒家

##### 法家
