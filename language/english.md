---

每天坚持记单词，练习说

---

<!-- tabs:start -->

#### **英语单词每天记忆**

  - [2022-04-12]
  > eager 英[ˈiːɡə(r)]   急切的、热切的、渴望的。 
I was eager to  get back to work  as soon as possible 我希望尽快回到工作岗位上 <br />
近义词  urgent 紧急的   she  had a more urgent errand(差事) 她有个重要的差事 <br />
相近的  earnest 认真的   The earnest doctor answered all our  questions 年轻的医生认真回到了我们所有的问题

  
<!-- tabs:end -->